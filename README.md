# README #

### What is this repository for? ###

* A small simulator for a group of Star Trek spaceships moving about in a finite universe, displayed in the console window by the first letter of what humanoid they represent; Klingon, Federation, Gorn, and Catullan. 
* The spaceship simulator operates in discrete turns. Each turn, every ship moves based on a movement pattern specific to the type of ship. When two or more ships are at the same location, some of the ships might be destroyed. The simulator runs indefinitely (until the user terminates the program), executing approximately one turn per second.

### How do I get set up? ###

* Run Main.java

### Authors ###

* Christopher Helmer
* Ken Meerdink

### Who do I talk to? ###

* Christopher Helmer
* christopher.helmer@yahoo.com