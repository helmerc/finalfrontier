/**
 * File: Catullan.java
 * Author: Christopher Helmer
 * version: TCSS 143, Spring 2013
 *
 * Description: No Changes have been made to this code
 */
public class Catullan extends SpaceShip {
	private boolean movingLeft;

	public Catullan(int row, int column) {
		super(row, column);
		movingLeft = random.nextBoolean();
	}

	public char getIcon() {
		return 'c';
	}

	public void move() {
		if (movingLeft && column == 0)
			movingLeft = false;
		else if (!movingLeft && column == FinalFrontier.WIDTH - 1)
			movingLeft = true;
		
		if (movingLeft)
			column--;
		else
			column++;
	}
}
