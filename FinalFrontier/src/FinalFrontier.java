import java.util.*;
/**
 * File: FinalFrontier.java
 * Author: Christopher Helmer
 * version: TCSS 143, Spring 2013
 *
 * Description: No Changes have been made to this code
 */
public class FinalFrontier extends TimerTask {
	public static final int WIDTH = 20;
	public static final int HEIGHT = 20;

	private SpaceShip[] spaceShips;
    private Random rand = new Random();

	public FinalFrontier(int catullans, int gorns, int klingons, int federations) {
		if (catullans < 0 || gorns < 0 || klingons < 0 || federations < 0)
			throw new IllegalArgumentException();

		spaceShips = new SpaceShip[catullans + gorns + klingons + federations];
		int index = 0;
		for (int i = 0; i < catullans; i++, index++)
			spaceShips[index] = new Catullan( rand.nextInt( HEIGHT ), rand.nextInt( WIDTH) );
		for (int i = 0; i < gorns; i++, index++)
			spaceShips[index] = new Gorn( rand.nextInt( HEIGHT ), rand.nextInt( WIDTH) );
		for (int i = 0; i < klingons; i++, index++)
			spaceShips[index] = new Klingon( rand.nextInt( HEIGHT ), rand.nextInt( WIDTH) );
		for (int i = 0; i < federations; i++, index++)
			spaceShips[index] = new Federation( rand.nextInt( HEIGHT ), rand.nextInt( WIDTH) );
	}

	public void run() {
		for (SpaceShip spaceShip : spaceShips)
			if (spaceShip.isUnderWay())
				spaceShip.move();

		for (int i = 0; i < spaceShips.length; i++) {
			if (spaceShips[i].isUnderWay())
				for (int j = i + 1; j < spaceShips.length; j++)
					if (spaceShips[j].isUnderWay() && spaceShips[i].isAtSameLocation(spaceShips[j])) {
						if (spaceShips[i].canDestroy(spaceShips[j])) {
							spaceShips[i].justDestroyed();
							spaceShips[j].isDestroyed();
							break;
						} else if (spaceShips[j].canDestroy(spaceShips[i])) {
							spaceShips[j].justDestroyed();
							spaceShips[i].isDestroyed();
							break;
						}
					}
		}

		System.out.println(this);
	}

	public String toString() {
        char[][] strSpace = new char[ WIDTH ][ HEIGHT ];
        for( int i = 0; i < WIDTH; i++ ) {
            for( int j = 0; j < HEIGHT; j++ ) {
                strSpace[ i ][ j ] = ' ';
            }
        }
        for( int i = 0 ; i < spaceShips.length; i++ ) {
            if( spaceShips[ i ].isUnderWay() ) {
                int r = spaceShips[ i ].getRow();
                int c = spaceShips[ i ].getColumn();
                strSpace[ r ][ c ] = spaceShips[ i ].getIcon();
            }
        }
        String outString = "+";
        for( int i = 0; i < WIDTH; i++ ) {
            outString += "-";
        }
        outString += "+\n";
        for( int i = 0; i < WIDTH; i++ ) {
            outString += "|";
            for( int j = 0; j < HEIGHT; j++ ) {
                outString += strSpace[ i ][ j ];
            }
            outString += "|\n";
        }
        outString += "+";
        for( int i = 0; i < WIDTH; i++ ) {
            outString += "-";
        }
        outString += "+\n";
		return outString;
	}
}
