import java.util.*;
/**
 * File: SpaceShip.java
 * Author: Christopher Helmer
 * version: TCSS 143, Spring 2013
 *
 * Description: This class develops the basic characteristics of all   
 *              spaceships.
 */
public abstract class SpaceShip {
	protected static final Random random = new Random();
	protected static final int HAS_WARP_DURATION = 15;
    
	protected int row;
	protected int column;
    
    protected int warpPower;//will be used for a countdown
                            //for how long the ship has warp power
                            
    protected boolean isDead;//boolean for dead spaceship
    
    protected boolean justKilled;//boolean for if spaceship just destroyed 
                                 //another spaceship
        
    /**
	 * Initializes a new SpaceShip object that resides at a specific location.
	 *
	 * @param row    The row at which the SpaceShip will be located.
	 * @param column The column at which the SpaceShip will be located.
     * @param isDead is set to false as default 
     * @param justKilled is set to false as default
     * @param warpPower is used for all spaceships as a counter for warp power
	 */
	public SpaceShip(int row, int column) {
		this.row = row;
		this.column = column;
        isDead = false;
        justKilled = false;
        warpPower = 0;
	}
    

	/**
	 * Returns the current row location of this spaceShip.
	 *
	 * @return the current row location of this spaceShip.
	 */
	public int getRow() {
		return row;
	}

	/**
	 * Returns the current column location of this spaceShip.
	 *
	 * @return the current column location of this spaceShip.
	 */
	public int getColumn() {
		return column;
	}

	/**
	 * Indicates whether two spaceShips reside at the same location.
	 *
	 * @param spaceShip A <code>SpaceShip</code> object.
	 *
	 * @return <code>true</code> if this spaceShip and the specified spaceShip
	 * reside at the same row and column, or <code>false</code> otherwise.
	 *
	 * @throws IllegalArgumentException if the specified spaceShip is
	 * <code>null</code>.
	 */
	public boolean isAtSameLocation(SpaceShip spaceShip) {
		if (spaceShip == null)
			throw new IllegalArgumentException();

		return row == spaceShip.row && column == spaceShip.column;
	}

	/**
	 * Indicates whether this spaceShip is under way, i.e., not destroyed.
	 *
	 * @return <code>true</code> if this spaceShip is alive, or
	 * <code>false</code> otherwise.
	 */
	public boolean isUnderWay() {
		if (isDead)
            return false;
        else
            return true;
	}

	/**
	 * Returns the character icon for this spaceShip.
	 *
	 * @return the character icon for this spaceShip.
	 */
	public char getIcon() {
		return '*';
	}

	/**
	 * Indicates whether this spaceShip can destroy the specified spaceShip.
	 *
	 * @param spaceShip A <code>SpaceShip</code> object.
	 *
	 * @return <code>true</code> if this spaceShip can destroy the specified spaceShip,
	 * or <code>false</code> otherwise.
	 *
	 * @throws IllegalArgumentException if the specified spaceShip is
	 * <code>null</code>.
	 */
	public boolean canDestroy(SpaceShip spaceShip) {
		return false;
	}

	/**
	 * Moves the spaceShip one space according to the appropriate species
	 * movement behavior.
	 */
	public void move() {
	}
    
    /**
     *Indicates whether the ship has Warp capabilities
     *
     * @return <code>true</code> if this spaceShip has warp capabilities,
	 * or <code>false</code> otherwise.
     */
    
    public boolean hasWarp() { 
        return !isDead && justKilled && warpPower > 0;
    }
    
     
    /**
	 * Called to indicate that this spaceShip has just destroyed another spaceShip
     * Changes the boolean expression characteristics of isDead and justKilled
     * @return void
	 */
	public void justDestroyed() {
        isDead = false;
        justKilled = true;
        warpPower = HAS_WARP_DURATION;
    }

	/**
	 * Called to indicate that this spaceShip has been destroyed and is no longer
	 * alive.
     * Changes the boolean expression characteristics of isDead and justKilled
     * @return void
	 */
	public void isDestroyed() {
        isDead = true;
        justKilled = false;
	}
}