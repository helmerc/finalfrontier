import java.util.*;
/**
 * File: Gorn.java
 * Author: Christopher Helmer
 * version: TCSS 143, Spring 2013
 *
 * Description: This class develops the characteristics of a   
 *              Gorn spaceship.
 */
public class Gorn extends SpaceShip {
	
    private boolean movingLeft;//boolean for initialization of L/R movement
    private boolean movingDown;//boolean for initialization of up/down mvmnt
    
        
    /**
     * Constructor that places the Gorn 
     * SpaceShips at specific locations and creates
     * random integers to start the movement 
     * of the ship.
     *
     * @param row    The row at which the SpaceShip will be located.
	 * @param column The column at which the SpaceShip will be located.
     * @param movingLeft is random boolean to accomodate horizontal movement
     * @param movingDown is random boolean to accomodate vertical movement        
     */    
    public Gorn(int row, int column) {
		super(row, column);
        movingLeft = random.nextBoolean();
        movingDown = random.nextBoolean();
        
	}
    
    /**
     * Provides an icon to represent the Gorn 
     * SpaceShip
     *
     * @return an icon to represent the Gorn ships 
     * uppercase if the ship has warp capabilities
     */
    
    public char getIcon() {
        if (hasWarp()) 
            return 'G';
        else 
            return 'g';
        
	}
    
    /**
     * Determines movement of the Gorn ship
     *
     * @return column and row position              
     */
	public void move() {
        Random myMove = new Random();
        int moveMe = myMove.nextInt(FinalFrontier.WIDTH - 1);
        
        if (movingLeft && column == 0) {
			movingLeft = false;
		}else if (!movingLeft && column == FinalFrontier.WIDTH - 1){
			movingLeft = true;
        }
           
        if (movingDown && row == 0){
            movingDown = false;
        }else if (!movingDown && row == FinalFrontier.HEIGHT - 1){
            movingDown = true;
        }
        
        if (movingDown && !movingLeft || movingDown){
            row--;
        }else if (!movingLeft && row <= moveMe)  {
            row++;
        }else if (movingLeft && !movingDown || movingLeft){
            column--;
        }else if (!movingDown && column <= moveMe){
            column++;
        }else if (!movingDown){
            row++;
        }else if (!movingLeft){
            column++;
        }
        warpPower--;//counts down so ship only has warp capabilities for 
                    //a duration equal to HAS_WARP_DURATION
    }
        
    /**
	 * Indicates whether this spaceShip can destroy the specified spaceShip.
	 *
	 * @param spaceShip A <code>SpaceShip</code> object.
	 *
	 * @return <code>true</code> if this spaceShip can destroy the specified spaceShip,
	 * or <code>false</code> otherwise.
	 *
	 * @throws IllegalArgumentException if the specified spaceShip is
	 * <code>null</code>.
	 */
	public boolean canDestroy(SpaceShip spaceShip) {
		return spaceShip instanceof Catullan;
	}
}