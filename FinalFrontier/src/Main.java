/**
 * @author Ken Meerdink
 */
import java.util.*;

public class Main {
	public static void main(String[] args) {
		FinalFrontier space = new FinalFrontier(10, 10, 10, 10);
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(space, 0, 1000);
	}
}
